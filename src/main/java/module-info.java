module gl1 {
	
	exports de.grogra.gl;
	exports de.grogra.glsl.utility;
	exports de.grogra.glsl.renderpass;
	exports de.grogra.glsl;
	exports de.grogra.glsl.light;
	exports de.grogra.glsl.renderable;
	exports de.grogra.glsl.material;
	exports de.grogra.glsl.renderpass.nostencil;
	exports de.grogra.glsl.renderable.vbo;
	exports de.grogra.glsl.light.shadow;
	exports de.grogra.glsl.material.channel;	
	
	requires java.desktop;
	
	requires imp;
	requires imp3d;
	requires vecmath;
	requires jogl;
	requires xl.core;
	requires java.logging;
	requires graph;
	requires platform.core;
	requires utilities;
	requires math;
	requires raytracer;
	requires platform;
	
	requires org.burningwave.core;
	
	
	opens de.grogra.gl to utilities;
	opens de.grogra.glsl to utilities;

}
