package de.grogra.glsl.light.shadow;

import de.grogra.glsl.light.GLSLAreaLight;
import de.grogra.glsl.light.GLSLSkyReflectionLight;
import de.grogra.glsl.utility.GLSLCollection;
import de.grogra.glsl.utility.GLSLManagedShader;

/**
 * Interface for all GLSLLightShaders.
 * @author Konni Hartmann
 */
public class ShadowLightCollection {
	private static final GLSLCollection col = new GLSLCollection();
	
	public static GLSLManagedShader getGLSLManagedObject(Object inp) {
		return col.getGLSLManagedObject(inp);
	}
	
	public static void initMap() {
		col.AddToMap(new GLSLSpotLightShadow());
		col.AddToMap(new GLSLPointLightShadow());
		col.AddToMap(new GLSLDirectionalLightShadow());
		col.AddToMap(new GLSLSkyLightShadow());
		col.AddToMap(new GLSLSkyReflectionLight());
		col.AddToMap(new GLSLAreaLight());
	}
}
