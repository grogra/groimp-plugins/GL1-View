package de.grogra.glsl.utility;

import javax.media.opengl.GL;

import de.grogra.glsl.OpenGLState;

public interface GLSLOpenGLObject {
	public void cleanup(OpenGLState glState, boolean javaOnly);
}
