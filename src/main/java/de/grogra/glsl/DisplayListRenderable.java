package de.grogra.glsl;

abstract class DisplayListRenderable
{
	abstract void render (float lod);
}
