package de.grogra.glsl.renderable.vbo;

import de.grogra.glsl.OpenGLState;
import de.grogra.glsl.utility.VertexBufferObject;

public class VBOManager {
	public static final int BOX = 0;
	
	private static final int size = 1;

	VertexBufferObject box = null;
	
	public VertexBufferObject getVBO(int index, OpenGLState glState) {
		if(box == null)
			box = new GLSLBoxVBO(glState);
		return box;
	}
	
}
