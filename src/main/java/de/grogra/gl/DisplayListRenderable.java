package de.grogra.gl;

abstract class DisplayListRenderable
{
	abstract void render (float lod);
}
