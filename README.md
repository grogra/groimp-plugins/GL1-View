# OpenGL 1 View Plugin

This plugin add support for an OpenGL 1 implementation of the 3D view. The view can be accessed from GroIMP in the 3D view, under "View>Display>OpenGL". 

Additionally, it provides an incomplete implementation of OpenGL 1 Shading Language for the 3d View. Which is registered as "OpenGL (Proteus)".
